from django.conf.urls import include,url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^profilelogin/', include('profilelogin.urls')),
    url(r'^$', include('profilelogin.urls')),
]
